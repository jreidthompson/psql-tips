    <div class="tip">You can use the <code>\g</code> metacommand to execute a query as an
    alternative to the semicolon character (;).
      <pre><code class="hljs bash">laetitia=# select * from test\g
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)</code></pre>This feature is available since at least Postgres 7.1.
	</div>
