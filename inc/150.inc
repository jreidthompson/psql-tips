    <div class="tip">
        The <code>\p</code> will print the current query buffer to the standard output.
	If the current query buffer is empty, the most recently executed query
	is printed instead.
      <pre><code class="hljs bash">laetitia=# \p
select * from test limit 5;</code></pre>This feature is available
at least since Postgres 7.1.
	</div>
