    <div class="tip">As the <code>\g</code> metacommand, the <code>\gx</code>
      metacommand can send the result of a query in a file.
      <pre><code class="hljs bash">laetitia=# select * from pg_settings where name = 'log_directory';
     name      | setting | unit |               category               |                  short_desc                   |                               extra_desc                                | context | vartype | source  | min_val | max_val | enumvals | boot_val | reset_val | sourcefile | sourceline | pending_restart 
---------------+---------+------+--------------------------------------+-----------------------------------------------+-------------------------------------------------------------------------+---------+---------+---------+---------+---------+----------+----------+-----------+------------+------------+-----------------
 log_directory | log     |      | Reporting and Logging / Where to Log | Sets the destination directory for log files. | Can be specified as relative to the data directory or as absolute path. | sighup  | string  | default |         |         |          | log      | log       |            |            | f
(1 row)

laetitia=# \gx log_directory.output
laetitia=# \! cat log_directory.output
-[ RECORD 1 ]---+------------------------------------------------------------------------
name            | log_directory
setting         | log
unit            | 
category        | Reporting and Logging / Where to Log
short_desc      | Sets the destination directory for log files.
extra_desc      | Can be specified as relative to the data directory or as absolute path.
context         | sighup
vartype         | string
source          | default
min_val         | 
max_val         | 
enumvals        | 
boot_val        | log
reset_val       | log
sourcefile      | 
sourceline      | 
pending_restart | f</code></pre>This feature is available since Postgres 10.
	</div>
